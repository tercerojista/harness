import zope.interface

from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import \
    presence_of_element_located
from selenium.webdriver.support.ui import WebDriverWait

from duckduckgo_harness.sdk.duck_duck_go import IDuckDuckGoPort


# default time to wait for a response in seconds
DEFAULT_WAIT_TIME = 10


class DuckDuckGoArithmetic(object):
    """
    Provides functionality necessary to communicate with DuckDuckGo
    via Web Interface.
    """
    zope.interface.implements(IDuckDuckGoPort)

    def __init__(self, context):
        self.context = context

    def get_search_result(self, search_term):
        self._go_to_search_page()
        self._enter_the_search_term(search_term)
        return self._fetch_the_result()

    def _go_to_search_page(self):
        self.context.browser.get('http://www.duckduckgo.com')

    def _enter_the_search_term(self, search_term):
        search_box = self.context.browser.find_element_by_id(
            'search_form_input_homepage'
        )
        search_button = self.context.browser.find_element_by_id(
            'search_button_homepage'
        )

        search_box.send_keys(search_term)
        search_button.click()

    def _fetch_the_result(self):
        wait = WebDriverWait(self.context.browser, DEFAULT_WAIT_TIME)
        return wait.until(
            presence_of_element_located(
                (By.CSS_SELECTOR, '#zci-answer')
            )
        )

    def parse_arithmetic_result(self, search_result):
        return search_result.find_element_by_id(
            'zci-answer'
        ).find_element_by_class_name(
            'zci__result'
        ).text
