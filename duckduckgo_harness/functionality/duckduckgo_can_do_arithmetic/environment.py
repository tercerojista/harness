import os
from selenium import webdriver

from duckduckgo_harness.adapters.api.duck_duck_go_api import\
    DuckDuckGoAPI
from duckduckgo_harness.adapters.selenium.duck_duck_go_arithmetic import\
    DuckDuckGoArithmetic
from duckduckgo_harness.sdk.duck_duck_go import DuckDuckGoSDK


class Placeholder(object):
    pass


def before_all(context):
    context.data = {}
    initialize_adapters(context)
    initialize_sdk(context)


def initialize_adapters(context):
    context.adapter = Placeholder()
    context.adapter.selenium = Placeholder()
    adapter_to_use = os.environ.get('ADAPTER', 'api')
    if adapter_to_use == 'www':
        profile = webdriver.FirefoxProfile()
        context.browser = webdriver.Firefox(firefox_profile=profile)
        adapter = DuckDuckGoArithmetic
    elif adapter_to_use == 'api':
        adapter = DuckDuckGoAPI
    else:
        raise Exception('Unknown adapter')
    context.adapter.selenium.duck_duck_go_arithmetic = adapter(
        context
    )


def initialize_sdk(context):
    context.sdk = Placeholder()
    context.sdk.duck_duck_go = DuckDuckGoSDK(
        context.adapter.selenium.duck_duck_go_arithmetic
    )


def after_all(context):
    if getattr(context, 'browser', None):
        context.browser.close()
