from behave import given, when, then


@given(u'that I search DuckDuckGo with term: "{search_term}"')
def search_duckduckgo(context, search_term):
    context.data['search_result'] = context.sdk.duck_duck_go.get_search_result(
        search_term
    )


@when(u'I parse the arithmetic result')
def fetch_result(context):
    context.data['result'] = context.sdk.duck_duck_go.parse_arithmetic_result(
        context.data['search_result']
    )


@then(u'the result is "{expected_result}"')
def check_result(context, expected_result):
    assert context.data['result'] == expected_result
