# THE HARNESS

## The situation:

1. Legacy system needs refactoring
2. Legacy system needs extension
3. Legacy system needs decoupling and modularization
4. Often, legacy system does not have a well defined interface


## The proposal:

### Harness of automated tests around the legacy system built on the official interface.

In order to start refactoring we must prove that the changes we make to the code, do not change its functionality. Similarly, before we extend the system with new functionality, we must ensure that we do not break the existing one.

As far as the official interface to the system is concerned - if we find ourselves in need of a test harness then we probably do not have a clearly defined and tested interface. We may take into consideration some of the documentation provided with the system, but this leads to a dangerous path of trusting that the documentation is in sync with the reality.

The type of testing that is about to be proposed is very close to functional testing. We are not interested in the internal modularization of the existing software. We want to make sure that it works as a whole. This will open a path for modularization or even split of the tested system into discrete services.

It is of crucial importance to be able to isolate tests from each other and from the possibility of random data getting into the system under test. We need to be in total, deterministic control over the system in order to reliably assert and analyze the outcomes of system changes during the tests.

### Use of ports and adapters in order to introduce seams between the tested system and its dependencies

It may be difficult to provide all the services necessary for a system to function. The tested system may rely on a 3rd party REST interface that cannot operate in a sandbox mode, hence preventing us from execution of calls without interfering with a live system. We can address this situation by applying a [ports and adapters][] pattern to our communication with that 3rd party REST API and inject a mock REST service providing a test double for the adapter.

Supporting such a mock may seem time consuming but is important for our understanding of how our system communicates with other systems.

### Use of ports and adapters in order to introduce seams between the testing and tested systems

The harness tests are independent from the system they are testing. They do not know anything about the internals of the system under test. They are oblivious to the persistence layer or architectural choices. They see only the outer shell of the system.

Sometimes the only outer shell that will be visible to us is a web page. Sometimes we will have access to a REST or RPC service. All these entry points are valid entry points for our tests.

Additionally we must make sure that the test cases we are building can operate as individual units. Initially we want to provide a test suite for the whole system but one of our goals is the extension and amendment of the system. We may be forced to dramatically change or even remove some of the tests we write. Hence, we must ensure that our test units do not depend on each other.

We will tap into the capabilities of our system with adapters specific to each existing interface. We may have an adapter talking to a web page and another one to the REST.

We need to organize these adapters so that the functionality they expose is unified. This will be achieved by the use of contracts. Each contract is effectively a port into which an adapter capable of implementing that port can be plugged. These ports lay the foundation for the future API facade of our system.


### Evolution of an Interim SDK providing a programmatic interface to the tested system

It would be a standard procedure to start the testing exercise with writing a test suite around an official API and to base the tests on documentation available. This is usually a luxury which is not available. Hence, there is a need to evolve an official API to a system. It is possible to make it a part of the process providing the harness of tests.

We do not know the outline of the system we are about to test. So we must embark on a journey to discover it. There usually will be an obvious entry point to the system. It may be a 'log in' page or some kind of registration form. Usually this is a good place to start with the test harness.

Starting with that we will discover that we need some other entry points in order to set up our tests. E.g. we might need to setup a user before we can test the login or password reminder functionality. This is a good thing. Our plans (at least initially) need to be very flexible. We must find the end of the spaghetti of functionality dependency so that we start testing at the very core fucntionality of the system.

Eventually we will end up with a series of adapters connecting us to existing implementation. These adapters, when fed to the SDK's ports, will allow us to provide a unified, programmatic interface to the system. A first step to the redefinition of implementations and the introduction of new APIs.


### Use of a ubiquitous language in order to provide a shared source of truth for:

We could define our harness as a series of programs executed against the existing entry points. It would be adequate but we can do better. We can base these programs on a business readable [domain specific language][]. This language would be a basis for exchange of information between:

* the requirements owners
* the programmers
* the testers
* the executable specification

A move towards this way of specifying requirements signals to the business the importance of successful communication during the software development process. As a matter of fact, the creation of executable requirements was one of founding tenets of the Extreme Programming movement.

Participation of all the stakeholders in this exercise is crucial for the test harness to be complete.

## The diagram

We are aiming to deliver a system implementing the following architecture:

![Diagram of the harness](https://bytebucket.org/vladekm/harness/raw/267d6c35de97fb96fb5d3b5e16897405154a705d/static/main_diagram.png?token=90ce4c265176edae73c12ddb8a8c9eb659dfc341)

# The example.

In order to illustrate the ideas mentioned in the preceding paragraphs, we are going to build a small proof of concept toy test harness around the search engine DuckDuckGo. We will be testing that the results of arithmetical operations which can be entered into DuckDuckGo's search actually return correct answers.

##Problem definition.

Gherkin example 1
```gherkin
Feature: DuckDuckGo can do arithmetic

Scenario: DuckDuckGo can add
GIVEN that I search DuckDuckGo with term: "2 + 2"
WHEN I parse the arithmetic result
THEN the result is "4"

Scenario: DuckDuckGo can substract
GIVEN that I search DuckDuckGo with term: "6 - 2"
WHEN I parse the arithmetic result
THEN the result is "4"

Scenario: DuckDuckGo can multiply
GIVEN that I search DuckDuckGo with term: "3 * 2"
WHEN I parse the arithmetic result
THEN the result is "6"

Scenario: DuckDuckGo can divide
GIVEN that I search DuckDuckGo with term: "8 / 2"
WHEN I parse the arithmetic result
THEN the result is "0.625"
```

The DSL we used in that example is called [Gherkin][]. Thanks to its similarity to English, it is easily understood by non-technical team members.

We encourage the reader to dig into Gherkin's syntax at a later date. For now we need to understand that feature files are divided into scenarios representing a different aspect of the feature. Each scenario comprises a number of steps following a Given/When/Then pattern which is respectively:

1. Set up of the initial system condition
2. Action carried out on the system
3. Analysis of the change of the system's state or values returned by it.

The 'feature file' we defined is quite naive and we could expand it with more examples to make sure that we are testing more possibilities.

Gherkin example 2
```gherkin
Feature: DuckDuckGo can do arithmetic

Scenario Outline: DuckDuckGo can add
    GIVEN that I search DuckDuckGo with term: "<term1> + <term2>"
    WHEN I parse the arithmetic result
    THEN the result is "<result>"
        Examples:
        |term1|term2|result|
        |2    |2    |4     |
        |3    |8    |11    |
        |123  |341  |464   |

Scenario Outline: DuckDuckGo can subtract
    GIVEN that I search DuckDuckGo with term: "<term1> - <term2>"
    WHEN I parse the arithmetic result
    THEN the result is "<result>"
        Examples:
        |term1|term2|result|
        |7    |2    |5     |
        |12   |3    |9     |
        |464  |341  |123   |

Scenario Outline: DuckDuckGo can multiply
    GIVEN that I search DuckDuckGo with term: "<term1> * <term2>"
    WHEN I parse the arithmetic result
    THEN the result is "<result>"
        Examples:
        |term1|term2|result|
        |3    |2    |6     |
        |8    |3    |24    |
        |8    |13   |104   |

Scenario Outline: DuckDuckGo can divide
    GIVEN that I search DuckDuckGo with term: "<term1> / <term2>"
    WHEN I parse the arithmetic result
    THEN the result is "<result>"
        Examples:
        |term1|term2|result|
        |12   |4    |3     |
        |8    |4    |2     |
        |104  |13   |8     |
```
We have expanded the list of examples that we are covering. We have also introduced another feature of Gherkin called 'Scenario Outline'; these are templates for scenarios which will be then parse the values from the 'Examples' section.

It is possible to further complicate the Gherkin file by elimination of the 4 scenario outlines but the author finds that the gains of concise expression of intent are slightly outweighed by increased syntactical complexity and some of the intended audience members, especially those non-technically inclined, may be feeling a bit overwhelmed with 'programmatic like' look of the resulting Gherkin.

Gherkin example 3
```gherkin
Feature: DuckDuckGo can do arithmetic

Scenario Outline: DuckDuckGo can add, subtract, multiple and divide
    GIVEN that I search DuckDuckGo with term: "<term1> <operator> <term2>"
    WHEN I parse the arithmetic result
    THEN the result is "<result>"
        Examples:
        |term1|operator|term2|result|
        |2    |+       |2    |4     |
        |3    |+       |8    |11    |
        |123  |+       |341  |464   |
        |6    |-       |2    |5     |
        |8    |-       |3    |9     |
        |464  |-       |341  |123   |
        |3    |*       |2    |6     |
        |8    |*       |3    |24    |
        |8    |*       |13   |104   |
        |8    |/       |2    |4     |
        |8    |/       |4    |2     |
        |104  |/       |13   |8     |

```

##The technology

We now need to choose a technology to translate these specifications into something executable. Gherkin was originally used in the Cucumber package written in Ruby. The author is more comfortable writing Python so is going to go ahead with the behave package (http://pythonhosted.org/behave/).

The initial structure of our test harness is going to be as follows

```
duckduckgo_harness/
    functionality/
        duckduckgo_can_do_arithmetic/
            features/
                duckduckgo_can_do_arithmetic.feature
            steps/
                steps.py
    environment.py
```

The 'functionality' directory holds all the features that we are testing. Each of those functionalities is named according to a pattern which allows us to quickly identify the nature of the functionality. In our case we have a 'duckduckgo_can_do_arithmetic' which holds a 'features' directory which contains our actual feature file as per behave's specification. The chosen convention is to give the feature file the same name as the functionality directory. We place the text constituting our executable specification in 'functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature' file.

The 'steps' directory is responsible for translation of each step present in the .feature files into a series of executable python commands.

The 'environment.py' file present in the root of the harness is responsible for setting the stage for the behave process. We'll keep it empty for now.

Please note that each directory under 'feature' is a standalone behave test suite. We want to isolate and decouple our test suits so that we can run them in parallel later.


After installing the virtual environment we can run our first behave command:

```
(harness)atlas:harness vlad$ behave duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/
```

Which is going to result in:

```gherkin
Feature: DuckDuckGo can do arithmetic # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:1

  Scenario Outline: DuckDuckGo can add                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:3
    Given that I search DuckDuckGo with term: "2 + 2" # None
    When I parse the arithmetic result                # None
    Then the result is "4"                    # None

  Scenario Outline: DuckDuckGo can add                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:3
    Given that I search DuckDuckGo with term: "3 + 8" # None
    When I parse the arithmetic result                # None
    Then the result's value is "11"                   # None

<more failed scenarios>
```

Interestingly the end of the output is going to contain a suggested steps implementation skeleton. We are going to ignore these as the suggestions would result in a lot of repetition or very similar steps. We are going to use a feature of behave allowing us to specify generic steps capable of accepting arguments.

We are creating our own steps.py file as:

```python
@given(u'that I search DuckDuckGo with term: "{search_term}"')
def step_impl(context, search_term):
    assert False


@when(u'I parse the arithmetic result')
def fetch_result(context):
    assert False


@then(u'the result is "{expected_result}"')
def check_result(context, expected_result):
    assert False
```

Running the test suite again we will see a bunch of failing tests.

```gherkin
(harness)atlas:harness vlad$ behave duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/
Feature: DuckDuckGo can do arithmetic # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:1

  Scenario Outline: DuckDuckGo can add                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:3
    Given that I search DuckDuckGo with term: "2 + 2" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:5 0.000s
      Traceback (most recent call last):
        File "/Users/vlad/.virtualenvs/harness/lib/python2.7/site-packages/behave/model.py", line 1173, in run
          match.run(runner.context)
        File "/Users/vlad/.virtualenvs/harness/lib/python2.7/site-packages/behave/model.py", line 1589, in run
          self.func(context, *args, **kwargs)
        File "/Users/vlad/projects/harness/duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py", line 7, in step_impl
          assert False
      AssertionError

    When I parse the arithmetic result'               # None
    Then the result is "4"                            # None

<more failing tests>

Failing scenarios:
  duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:3  DuckDuckGo can add
  duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:3  DuckDuckGo can add

<more failing tests summary>
```

This is a good thing. It means that our gherkin is syntactically correct but the tests are not passing. Now we need to modify the steps to actually do something useful.

We need a way of communicating with DuckDuckGo in order to execute the command and fetch the result. In classical BDD approach this would be done directly in the steps. We want to get out some more from it and will opt for a ports/adapters architecture. We will write an adapter to talk to DuckDuckGo over http/rest(ish) way and plug that adapter into our Interim SDK.

##The adapter

Let's start with the adapter. We have chosen to implement it in Selenium/webdriver. We are going to add a new folder in our project as per:

```
duckduckgo_harness/
    adapters/
        selenium/
            duck_duck_go_arithmetic.py
    functionality/
        duckduckgo_can_do_arithmetic/
            features/
                duckduckgo_can_do_arithmetic.feature
            steps/
                steps.py
    environment.py
```

The new file ('adapters/selenium/duck_duck_go_arithmetic.py') is going to hold all the code necessary for any communication with our remote service and for processing of the response. The steps are going to use these functions to do their work.

We implement the adapter thus:

```python
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import \
    presence_of_element_located
from selenium.webdriver.support.ui import WebDriverWait


# default time to wait for a response in seconds
DEFAULT_WAIT_TIME = 10


def get_search_result(browser, search_term):
    _go_to_search_page(browser)
    _enter_the_search_term(browser, search_term)
    return _fetch_the_result(browser)


def _go_to_search_page(browser):
    browser.get('http://www.duckduckgo.com')


def _enter_the_search_term(browser, search_term):
    search_box = browser.find_element_by_id('search_form_input_homepage')
    search_button = browser.find_element_by_id('search_button_homepage')

    search_box.send_keys(search_term)
    search_button.click()


def _fetch_the_result(browser):
    wait = WebDriverWait(browser, DEFAULT_WAIT_TIME)
    return wait.until(
        presence_of_element_located(
            (By.CSS_SELECTOR, '#zci-answer')
        )
    )
    

def parse_arithmetic_result(search_result):
    return search_result.find_element_by_id(
        'zci-answer'
    ).find_element_by_class_name(
        'zci__result'
    ).text
```

and we use those functions in steps:

```python
from behave import given, when, then
from duckduckgo_harness.adapters.selenium.duck_duck_go_arithmetic import (
    get_search_result,
    parse_arithmetic_result
)


@given(u'that I search DuckDuckGo with term: "{search_term}"')
def search_duckduckgo(context, search_term):
    context.data['search_result'] = get_search_result(
        context.browser,
        search_term
    )


@when(u'I parse the arithmetic result')
def fetch_result(context):
    context.data['result'] = parse_arithmetic_result(
        context.data['search_result']
    )


@then(u'the result\'s value is "{expected_result}"')
def check_result(context, expected_result):
    assert context.data['result'] == expected_result
```

At this point in time we have a functioning test suite:

```gherkin
(harness)atlas:harness vlad$ behave duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/ --no-capture
Feature: DuckDuckGo can do arithmetic # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:1

  Scenario Outline: DuckDuckGo can add                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:3
    Given that I search DuckDuckGo with term: "2 + 2" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:8 2.398s
    When I parse the arithmetic result                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:16 0.031s
    Then the result is "4"                            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:23 0.000s

  Scenario Outline: DuckDuckGo can add                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:3
    Given that I search DuckDuckGo with term: "3 + 8" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:8 2.505s
    When I parse the arithmetic result                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:16 0.044s
    Then the result is "11"                           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:23 0.000s

  Scenario Outline: DuckDuckGo can add                    # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:3
    Given that I search DuckDuckGo with term: "123 + 341" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:8 2.244s
    When I parse the arithmetic result                    # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:16 0.033s
    Then the result is "464"                              # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:23 0.000s

  Scenario Outline: DuckDuckGo can subtract           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:13
    Given that I search DuckDuckGo with term: "7 - 2" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:8 2.714s
    When I parse the arithmetic result                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:16 0.033s
    Then the result is "5"                            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:23 0.000s

  Scenario Outline: DuckDuckGo can subtract            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:13
    Given that I search DuckDuckGo with term: "12 - 3" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:8 2.134s
    When I parse the arithmetic result                 # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:16 0.029s
    Then the result is "9"                             # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:23 0.000s

  Scenario Outline: DuckDuckGo can subtract               # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:13
    Given that I search DuckDuckGo with term: "464 - 341" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:8 2.092s
    When I parse the arithmetic result                    # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:16 0.029s
    Then the result is "123"                              # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:23 0.000s

  Scenario Outline: DuckDuckGo can multiply           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:23
    Given that I search DuckDuckGo with term: "3 * 2" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:8 2.201s
    When I parse the arithmetic result                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:16 0.028s
    Then the result is "6"                            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:23 0.000s

  Scenario Outline: DuckDuckGo can multiply           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:23
    Given that I search DuckDuckGo with term: "8 * 3" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:8 1.832s
    When I parse the arithmetic result                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:16 0.023s
    Then the result is "24"                           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:23 0.000s

  Scenario Outline: DuckDuckGo can multiply            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:23
    Given that I search DuckDuckGo with term: "8 * 13" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:8 2.442s
    When I parse the arithmetic result                 # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:16 0.030s
    Then the result is "104"                           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:23 0.000s

  Scenario Outline: DuckDuckGo can multiply            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:33
    Given that I search DuckDuckGo with term: "12 / 4" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:8 1.889s
    When I parse the arithmetic result                 # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:16 0.027s
    Then the result is "3"                             # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:23 0.000s

  Scenario Outline: DuckDuckGo can multiply           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:33
    Given that I search DuckDuckGo with term: "8 / 4" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:8 1.941s
    When I parse the arithmetic result                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:16 0.027s
    Then the result is "2"                            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:23 0.000s

  Scenario Outline: DuckDuckGo can multiply              # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:33
    Given that I search DuckDuckGo with term: "104 / 13" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:8 2.128s
    When I parse the arithmetic result                   # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:16 0.029s
    Then the result is "8"                               # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:23 0.000s

1 feature passed, 0 failed, 0 skipped
12 scenarios passed, 0 failed, 0 skipped
36 steps passed, 0 failed, 0 skipped, 0 undefined
Took 0m26.887s
```

Our test suite is implemented following the tenets we have defined in the first part of the article:

1. The steps use an adapter to communicate with the system under test.
2. The adapter isolates the steps from any unnecessary knowledge and/or technology.
3. The adapter contains all the implementation details and exposes the functionality with help of two 'public' methods.

## The SDK

At this point we have a working test suite and we have built an adapter which exposes the functionality of the system we are testing. Interestingly, this adapter sounds a lot like an official interface to the system. It does contain some 'private' and 'interface-centric' stuff. 

We might want to try to standardize this interface and make sure that it is properly documented. We are going to create an Interim SDK for that purpose. An important idea that we have to keep in mind is that the creation of this SDK is driven by a necessity to use its methods in tests we are writing. No method can appear here unless proven necessary by one of the 'given' or 'then' steps in our test suite.

Some refactoring will be necessary in order to make life a bit easier. The adapter will become a class and will be instantiated when the context namespace for the test suite is populated.


We are going to put the SDK at the same level as the adapter. Our new file structure:

```
duckduckgo_harness/
    adapters/
        selenium/
            duck_duck_go_arithmetic.py
    functionality/
        duckduckgo_can_do_arithmetic/
            features/
                duckduckgo_can_do_arithmetic.feature
            steps/
                steps.py
    sdk/
        duck_duck_go_arithmetic.py
    environment.py
```

and the content of the new file:


```python
import zope.interface
from zope.interface.verify import verifyClass


class IDuckDuckGoPort(zope.interface.Interface):
    def get_search_result(search_term):
        """Return search results"""

    def parse_arithmetic_result(search_result):
        """Extracts the arithmetic result from the search results"""


class DuckDuckGoSDK(object):

    def __init__(self, adapter):
        verifyClass(IDuckDuckGoPort, adapter.__class__)
        self.adapter = adapter

    def get_search_result(self, search_term):
        return self.adapter.get_search_result(search_term)
```
As a rule of thumb we are only elevating to the SDK the adapter method used in the 'GIVEN' step. The reason for this is that this method would be most likely to be used in other tests. Multiple reuse suggests a public interface.

It is also worth stopping here to explain some of the implementation choices. In order to be able to use the [ports and adapters][] pattern we must provide a definition of the communication (interface) so that adapters know what to implement. The important bits here are the definition of the communication and the possibility of switching the adapters (more on that in the next section).

We also need to refactor the steps, the adapter and the environment build script.


## The adapter switch

Once we have the SDK in place it is time to demonstrate the flexibility of the system. We are going to provide another adapter following our SDK contract. It turns out that there is a [duckduckgo module][] that allows communication with the DuckDuckGo API. We will install it and try using it in a separate adapter. Hopefuly, using the API instead of a browser and the HTML interface will speed the things up.

Our new structure:
```
duckduckgo_harness/
    adapters/
        selenium/
            duck_duck_go_arithmetic.py
        api/
            duck_duck_go_api.py
    functionality/
        duckduckgo_can_do_arithmetic/
            features/
                duckduckgo_can_do_arithmetic.feature
            steps/
                steps.py
    sdk/
        duck_duck_go_arithmetic.py
    environment.py
```

The new adapter implementation:

```
from HTMLParser import HTMLParser
import zope.interface
import duckduckgo
from duckduckgo_harness.sdk.duck_duck_go import IDuckDuckGoPort


class DuckDuckGoAPI(object):
    zope.interface.implements(IDuckDuckGoPort)

    def __init__(self, context):
        self.context = context

    def get_search_result(self, search_term):
        return duckduckgo.query(search_term)

    def parse_arithmetic_result(self, search_result):
        return strip_tags(search_result.answer.text)


class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []

    def handle_data(self, d):
        self.fed.append(d)

    def get_data(self):
        return ''.join(self.fed)


def strip_tags(html):
    s = MLStripper()
    s.feed(html)
    return s.get_data()

```

The only change required in the previous code is the amendment of the context namespace creation to use the new adapter rather than the old one. We run the test suite again:

```gherkin
(harness)atlas:harness vlad$ behave duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/ --no-capture
Feature: DuckDuckGo can do arithmetic # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:1

  Scenario Outline: DuckDuckGo can add                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:3
    Given that I search DuckDuckGo with term: "2 + 2" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:4 0.126s
    When I parse the arithmetic result                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:11 0.001s
    Then the result is "4"                            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:18 0.000s

  Scenario Outline: DuckDuckGo can add                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:3
    Given that I search DuckDuckGo with term: "3 + 8" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:4 0.090s
    When I parse the arithmetic result                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:11 0.000s
    Then the result is "11"                           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:18 0.000s

  Scenario Outline: DuckDuckGo can add                    # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:3
    Given that I search DuckDuckGo with term: "123 + 341" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:4 0.078s
    When I parse the arithmetic result                    # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:11 0.000s
    Then the result is "464"                              # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:18 0.000s

  Scenario Outline: DuckDuckGo can subtract           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:13
    Given that I search DuckDuckGo with term: "7 - 2" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:4 0.070s
    When I parse the arithmetic result                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:11 0.000s
    Then the result is "5"                            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:18 0.000s

  Scenario Outline: DuckDuckGo can subtract            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:13
    Given that I search DuckDuckGo with term: "12 - 3" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:4 0.079s
    When I parse the arithmetic result                 # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:11 0.000s
    Then the result is "9"                             # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:18 0.000s

  Scenario Outline: DuckDuckGo can subtract               # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:13
    Given that I search DuckDuckGo with term: "464 - 341" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:4 0.079s
    When I parse the arithmetic result                    # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:11 0.000s
    Then the result is "123"                              # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:18 0.000s

  Scenario Outline: DuckDuckGo can multiply           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:23
    Given that I search DuckDuckGo with term: "3 * 2" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:4 0.072s
    When I parse the arithmetic result                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:11 0.000s
    Then the result is "6"                            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:18 0.000s

  Scenario Outline: DuckDuckGo can multiply           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:23
    Given that I search DuckDuckGo with term: "8 * 3" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:4 0.079s
    When I parse the arithmetic result                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:11 0.000s
    Then the result is "24"                           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:18 0.000s

  Scenario Outline: DuckDuckGo can multiply            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:23
    Given that I search DuckDuckGo with term: "8 * 13" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:4 0.074s
    When I parse the arithmetic result                 # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:11 0.000s
    Then the result is "104"                           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:18 0.000s

  Scenario Outline: DuckDuckGo can multiply            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:33
    Given that I search DuckDuckGo with term: "12 / 4" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:4 0.081s
    When I parse the arithmetic result                 # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:11 0.001s
    Then the result is "3"                             # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:18 0.000s

  Scenario Outline: DuckDuckGo can multiply           # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:33
    Given that I search DuckDuckGo with term: "8 / 4" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:4 0.075s
    When I parse the arithmetic result                # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:11 0.000s
    Then the result is "2"                            # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:18 0.000s

  Scenario Outline: DuckDuckGo can multiply              # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/features/duckduckgo_can_do_arithmetic.feature:33
    Given that I search DuckDuckGo with term: "104 / 13" # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:4 0.072s
    When I parse the arithmetic result                   # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:11 0.000s
    Then the result is "8"                               # duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/steps/steps.py:18 0.000s

1 feature passed, 0 failed, 0 skipped
12 scenarios passed, 0 failed, 0 skipped
36 steps passed, 0 failed, 0 skipped, 0 undefined
Took 0m0.983s
```

The result is that the test executes now in under a second as opposed to previous 20-30 seconds. We have managed to leverage a more effective way of accessing the system once we have discovered/created it.

It is possible to test both adapters by providing a correct environment variable (ADAPTER=www or ADAPTER=api with api being the default one):

```
$ ADAPTER=api behave duckduckgo_harness/functionality/duckduckgo_can_do_arithmetic/
```


# The conclusion

This example may appear a bit trivial but it has allowed us to demonstrate all the main tenets of this testing approach.

We have driven our testing approach from an existing untested WWW/HTML interface. By sticking to a few simple rules we ended up with a system that provided us with:

* [domain specific language][]
* test coverage allowing us to start refactoring the system under test
* an interim SDK allowing us to interface with our system programmatically
* two adapters exposing different access points to our system under test


This is a purely functional test approach. In the next article we will present how front-ends/interfaces can be tested using the same approach and how both vectors of test can co-operate with each other.

---
[ports and adapters]: http://alistair.cockburn.us/Hexagonal+architecture 'Ports and Adapters'
[domain specific language]: http://martinfowler.com/bliki/BusinessReadableDSL.html 'Domain Specific Language'
[Gherkin]: https://github.com/cucumber/cucumber/wiki/Gherkin 'Gherkin'
[duckduckgo module]: https://pypi.python.org/pypi/duckduckgo2/ 'duckduckgo2 module'
